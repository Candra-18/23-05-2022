import React from "react";
import ExpensesForm from "./ExpensesForm";

import "./NewExpense.css";

const NewExpense = () => {
  return (
    <div className="new-expense">
      <form>
        <ExpensesForm/>
      </form>
    </div>
  );
};

export default NewExpense;
